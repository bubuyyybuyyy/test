// _app.js
import 'uikit/dist/css/uikit.min.css';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

UIkit.use(Icons);

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp;
