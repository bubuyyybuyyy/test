// pages/index.js
import React from 'react';

const Home = () => {
  const articles = [
    { title: 'Artikel 1', content: 'Konten artikel 1', photo: 'https://via.placeholder.com/150' },
    { title: 'Artikel 2', content: 'Konten artikel 2', photo: 'https://via.placeholder.com/150' },
    { title: 'Artikel 3', content: 'Konten artikel 3', photo: 'https://via.placeholder.com/150' },
  ];

  const sliderImages = [
    { src: 'https://via.placeholder.com/800x400', caption: 'Gambar 1' },
    { src: 'https://via.placeholder.com/800x400', caption: 'Gambar 2' },
    { src: 'https://via.placeholder.com/800x400', caption: 'Gambar 3' },
    { src: 'https://via.placeholder.com/800x400', caption: 'Gambar 4' },
    { src: 'https://via.placeholder.com/800x400', caption: 'Gambar 5' },
  ];

  return (
    <div>
      {/* Topbar */}
      <div className="uk-navbar-container uk-navbar-primary" uk-navbar="true" style={{ backgroundColor: '#1E3A8A' }}>
        <div className="uk-navbar-left">
          <ul className="uk-navbar-nav">
            <li><a href="/" className="uk-text-bold" style={{ color: '#ffffff' }}>Home</a></li>
            <li><a href="/karyawan" className="uk-text-bold" style={{ color: '#ffffff' }}>Karyawan</a></li>
          </ul>
        </div>
      </div>

      {/* Slider */}
      <div className="uk-position-relative uk-visible-toggle uk-light" uk-slideshow="true">
        <ul className="uk-slideshow-items">
          {sliderImages.map((image, index) => (
            <li key={index}>
              <img src={image.src} alt={`Slide ${index + 1}`} uk-cover="true" />
              <div className="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                <p>{image.caption}</p>
              </div>
            </li>
          ))}
        </ul>
        <a className="uk-slidenav-large uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous="true" uk-slideshow-item="previous"></a>
        <a className="uk-slidenav-large uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next="true" uk-slideshow-item="next"></a>
      </div>

      {/* Daftar Artikel */}
      <div className="uk-section uk-section-default">
        <div className="uk-container">
          <h2 className="uk-heading-line uk-text-center"><span>Daftar Artikel</span></h2>
          <div className="uk-grid-match uk-child-width-1-3@m" uk-grid="true">
            {articles.map((article, index) => (
              <div key={index}>
                <div className="uk-card uk-card-hover uk-card-body uk-light" style={{ backgroundColor: '#1E3A8A' }}>
                  <div className="uk-card-media-top">
                    <img src={article.photo} alt={`Photo of ${article.title}`} />
                  </div>
                  <div className="uk-card-body">
                    <h3 className="uk-card-title">{article.title}</h3>
                    <p>{article.content}</p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>

      {/* Galeri */}
      <div className="uk-section uk-section-muted" style={{ backgroundColor: '#f0f0f0' }}>
        <div className="uk-container">
          <h2 className="uk-heading-line uk-text-center"><span>Galeri</span></h2>
          <div className="uk-grid-small uk-child-width-1-4@m" uk-grid="true">
            {[1, 2, 3, 4].map((item, index) => (
              <div key={index}>
                <div className="uk-card uk-card-default uk-card-body">
                  <img src={`https://via.placeholder.com/200`} alt={`Gallery Item ${item}`} />
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
