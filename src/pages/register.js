import React, { useRef, useState } from "react";
import Head from "next/head";
import Link from "next/link";
import UserNetwork from "@/network/userNetwork";

export default function Index() {
  const formRef = useRef();
  const [errors, setErrors] = useState({});
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [phone, setPhone] = useState('');

  const validateInputs = () => {
    const newErrors = {};
    if (!name.trim()) {
        newErrors.name = "name harus diisi";
    }
    if (!email.trim()) {
        newErrors.email = "Email harus diisi";
    } else if (email.length > 255) {
        newErrors.email = "Email terlalu panjang";
    } else if (!/\S+@\S+\.\S+/.test(email)) {
        newErrors.email = "Email tidak valid";
    }
    if (!password.trim()) {
        newErrors.password = "Password harus diisi";
    }
    if (!confirmPassword.trim()) {
        newErrors.confirmPassword = "Confirm Password harus diisi";
    }
    if (!phone.trim()) {
        newErrors.phone = "Phone harus diisi";
    }
    return newErrors;
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const newErrors = validateInputs();
    if (Object.keys(newErrors).length === 0) {
      const submit = {
        name:name,
        email: email,
        password: password,
        password_confirmation:confirmPassword,
        phone:phone,
      };

      const response = await UserNetwork.registrasi(submit);
      return response;
    } else {
      setErrors(newErrors);
    }
  }

  return (
    <div>
      <Head>
        <title>Registrasi</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.7.3/css/uikit.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.7.3/js/uikit.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.7.3/js/uikit-icons.min.js"></script>
      </Head>

      <div className="uk-flex uk-flex-center uk-flex-middle uk-height-viewport uk-background-">
        <div className="uk-width-medium uk-padding uk-background-muted">
          <h2 className="uk-text-center">Registrasi</h2>
          <form className="uk-form-stacked" ref={formRef} onSubmit={handleSubmit}>
            <div className="uk-margin">
              <label className="uk-form-label" htmlFor="name">Name:</label>
              <div className="uk-form-controls">
                <input
                  className="uk-input"
                  id="name"
                  type="text"
                  placeholder="name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
                {errors.name && <p className="uk-text-danger">{errors.name}</p>}
              </div>
            </div>
            <div className="uk-margin">
              <label className="uk-form-label" htmlFor="email">Email:</label>
              <div className="uk-form-controls">
                <input
                  className="uk-input"
                  id="email"
                  type="text"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {errors.email && <p className="uk-text-danger">{errors.email}</p>}
              </div>
            </div>
            <div className="uk-margin">
              <label className="uk-form-label" htmlFor="password">Password:</label>
              <div className="uk-form-controls">
                <input
                  className="uk-input"
                  id="password"
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                {errors.password && <p className="uk-text-danger">{errors.password}</p>}
              </div>
            </div>
            <div className="uk-margin">
              <label className="uk-form-label" htmlFor="password">Confirm Password:</label>
              <div className="uk-form-controls">
                <input
                  className="uk-input"
                  id="password"
                  type="confirmPassword"
                  placeholder="confirm Password"
                  value={confirmPassword}
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
                {errors.confirmPassword && <p className="uk-text-danger">{errors.confirmPassword}</p>}
              </div>
            </div>
            <div className="uk-margin">
              <label className="uk-form-label" htmlFor="phone">Phone:</label>
              <div className="uk-form-controls">
                <input
                  className="uk-input"
                  id="phone"
                  type="text"
                  placeholder="phone"
                  value={phone}
                  onChange={(e) => setPhone(e.target.value)}
                />
                {errors.phone && <p className="uk-text-danger">{errors.phone}</p>}
              </div>
            </div>
            <div className="uk-margin uk-text-center">
              <button className="uk-button uk-button-primary" type="submit">Registrasi</button>
            </div>
          </form>
          <p className="uk-text-center">Belum punya akun? <Link href="/register">Daftar di sini</Link></p>
        </div>
      </div>
    </div>
  );
}
