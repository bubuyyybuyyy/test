import axios, { Axios } from "axios";

    const login = async (submit) => {
        const response = await axios.post(`http://localhost:8000/api/login`, submit);
        return response;
    };

    const registrasi = async (submit) => {
        const response = await axios.post(`http://localhost:8000/api/register`, submit);
        return response;
    }
    
    const UserNetwork = {
        login,
        registrasi,
    };
  
export default UserNetwork;